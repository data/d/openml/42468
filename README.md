# OpenML dataset: hls4ml_lhc_jets_hlf

https://www.openml.org/d/42468

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Identify jets of particles from the LHC, created for the study of ultra low latency inference with hls4ml.
Use 16 high level features to identify the 5 jet classes: quark (q), gluon (g), W boson (w), Z boson (z), or top quark (t).


The hls4ml paper: https://iopscience.iop.org/article/10.1088/1748-0221/13/07/P07027
The dataset DOI: https://doi.org/10.5281/zenodo.3602260

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42468) of an [OpenML dataset](https://www.openml.org/d/42468). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42468/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42468/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42468/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

